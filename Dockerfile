FROM openjdk:11
LABEL maintainer="anders.harrisson@ess.eu"

ARG JAR_FILE=target/*.jar

ENV LDAP_ENABLED=false \
    DEMO_AUTH_ENABLED=true

COPY ${JAR_FILE} app.jar
CMD ["java", "-jar", "/app.jar", "-Dldap.enabled=$LDAP_ENABLED", "-Ddemo_auth.enabled=$DEMO_AUTH_ENABLED"]
